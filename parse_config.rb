
def print_usage
  puts "Usage: ruby parse_config.rb /path/to/config/file"
end

def parse_file(file_to_parse)
  #open the file for reading
  config_file = File.new(file_to_parse, "r")
  #create a hash to store the parsed output
  config_opts = {}
  #loop over each line in the file
  while (line = config_file.gets)
    #parse the line if its not a comment line
    if !is_comment?(line)
      #split based on = sign
      key,val = line.split("=")
      #dont add values to hash if they are empty
      if !key.strip.empty? && !val.strip.empty?
        config_opts[key.strip] = val.strip
      end
    end
  end
  config_opts
end

#tells if the line is a comment line
def is_comment?(line)
  !line.empty? && line.strip.start_with?("#")
end

begin
  #grab the filename from the command line
  file_to_parse = ARGV[0]
  #check to see if file exists
  if file_to_parse.nil?
    #file not found
    print_usage
    exit
  elsif !File.exists?(file_to_parse)
    #print error message with var value
    puts "File does not exist. Please check location: #{file_to_parse}"
    exit
  else
    config_opts = parse_file(file_to_parse)
    puts "Configuration:"
    #print out each key val pair in the hash
    config_opts.each do |key, val|
      puts "#{key}: #{val}"
    end
  end
end
